#!/bin/bash

# function aptUpdate (){
# apt-get update -y

# }

# function aptInstall () {
# serviceName=$1
# apt-get install $serviceName -y
# }

 function downloadService () {
 URL=$1
 wget $URL

}

OS=$(cat /etc/os-release | grep "^NAME" | awk -F'"' '{print $2}')
version=$2


if [[ $OS == "Ubuntu" ]]
    then

    apt-get update  -y 
    apt-get upgrade -y 
    sudo apt-get install build-essential make zlib1g-dev libpcre2-dev libevent-dev libssl-dev -y
    apt-get install libz-dev -y
    apt-get install build-essential zlib1g-dev -y 
    apt-get install libpcre2-dev -y 
    apt-get install libsqlite3-dev -y 

    downloadService https://github.com/ossec/ossec-hids/archive/refs/tags/3.6.0.tar.gz
    tar -zxvf 3.6.0.tar.gz
    cd ossec-hids-3.6.0
    ./install.sh

else

echo "Your system is not suitable for the script to run"
fi

